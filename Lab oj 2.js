function Book(name, title, pages, price){
    this.authorName = name;
    this.title = title;
    this.nberPages = pages;
    this.price = price;
    this.showInfo = function(){
        console.log(`Author: ${this.authorName} Title: ${this.title} 
        Pages: ${this.nberPages} Price: ${this.price}`);
        return this;
    }
}

let book1 = new Book('smith', 'into to java', 120,59.9);
let book2 = new Book('sara', 'into to javascript', 100,49.9);
let book3 = new Book('james', 'basic of AI', 300,79.9);
let book4 = new Book('ali', 'coding with C++', 250,19.9);
let lisBooks = [book1,book2,book3,book4];
function sortBook_Author(){
    lisBooks.sort(function compare(a,b){
        if(a.authorName>b.authorName){
            console.log(1);
            return 1;
        }
        else if(a.authorName<b.authorName){
            console.log(-1);
            return -1
        }
        else{
            console.log(0);
            return 0;
        }
    });
}
function sortBook_Title(){
    lisBooks.sort(function compare(a,b){
        if(a.title>b.title){
            console.log(1);
            return 1;
        }
        else if(a.title<b.title){
            console.log(-1);
            return -1
        }
        else{
            console.log(0);
            return 0;
        }
    });
}
function sortBook_Price(){
    lisBooks.sort(function compare(a,b){
        if(a.price>b.price){
            console.log(1);
            return 1;
        }
        else if(a.price<b.price){
            console.log(-1);
            return -1
        }
        else{
            console.log(0);
            return 0;
        }
    });
}
function sortBook_NberPages(){
    lisBooks.sort(function compare(a,b){
        if(a.nberPages>b.nberPages){
            console.log(1);
            return 1;
        }
        else if(a.nberPages<b.nberPages){
            console.log(-1);
            return -1
        }
        else{
            console.log(0);
            return 0;
        }
    });
}
