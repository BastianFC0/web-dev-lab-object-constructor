function Counter (count, step) {
    this.count = count;
    this.step = step;
    this.increment = function(){
        this.count = this.count+this.step;
        return this;
    }
    this.decrement = function(){
        this.count = this.count-this.step;
        return this;
    }
    this.show = function(){
        console.log("Count: "+this.count+" Step:"+this.step);
        return this;
    }
}

let counter1 = new Counter(10, 2);
